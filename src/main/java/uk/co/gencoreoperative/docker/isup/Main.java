package uk.co.gencoreoperative.docker.isup;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Command line utility which will block until a single HTTP resource is available
 * and responds 200.
 */
public class Main {
    @Parameter(required = false, description = "<hostname>", validateWith = HostnameValidator.class)
    private List<String> hostname = new ArrayList<>();

    @Parameter(names = {"-h", "--help"}, description = "This help text", help = true)
    private boolean help = false;

    @Parameter(names = {"-d", "--duration"}, description = "The number of seconds to poll for, -1 is infinite", required = false)
    private int poll = -1;

    @Parameter(names = {"-t", "--timeout"}, description = "The number of milliseconds to wait for a request before timeout", required = false)
    private int timeout = 100;

    private final IsUp util;

    public Main() {
        util = new IsUp(timeout);
    }

    public List<URL> getURLs() {
        List<URL> urls = new ArrayList<>();
        for (String u : hostname) {
            try {
                urls.add(new URL(u));
            } catch (MalformedURLException e) {
                throw new IllegalStateException(e);
            }
        }
        return urls;
    }

    /**
     * Poll the URL for the configured time.
     * @return True if the URL was up, false if not and the timeout expired.
     */
    public Result getResultFromURL(URL url) {
        Result result = new Result();
        try {
            return util.isUp(url).get(timeout, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        } catch (ExecutionException | TimeoutException e) {
            result.timeout();
        }
        return result;
    }

    /**
     * Perform a check against the given URL.
     *
     * The result can be in one of a number of states:
     *
     * Result#OK If the server responded with a 200 status code.
     * Result#FAILED If the server responded but it wasn't a 200 code.
     * Result#ERROR If there was a problem connecting to the server.
     * Result#TIMEOut If the request timed out connecting or reading from the server.
     *
     * @param url Non null
     * @return Non null
     */
    public Result perform(URL url) {
        Result result;
        long start = System.currentTimeMillis();
        do {
            result = getResultFromURL(url);
            if (result.isSuccess()) {
                break;
            }
        } while (!hasDurationElapsed(start));
        return result;
    }

    private boolean hasDurationElapsed(long start) {
        if (poll == -1) {
            return false;
        }
        long elapsedSeconds = (System.currentTimeMillis() - start) / 1000;
        return elapsedSeconds >= poll;
    }


    public static void main(String... args) {
        Main main = new Main();
        JCommander commander = new JCommander(main);
        commander.setProgramName("is-up");

        boolean error = false;
        try {
            commander.parse(args);
        } catch (ParameterException e) {
            error = true;
        }

        // Validate processed arguments
        if (main.getURLs().isEmpty()) error = true;

        if (main.help || error) {
            commander.usage();
            System.exit(-1);
        }

        URL url = main.getURLs().get(0);
        Result result = main.perform(url);

        System.out.println(MessageFormat.format(
                "{0} is {1}",
                url,
                result));
        System.exit(result.isSuccess() ? 0 : 1);
    }

    public static class HostnameValidator implements IParameterValidator {
        @Override
        public void validate(String key, String value) throws ParameterException {
            String url = value;
            try {
                new URL(url);
            } catch (MalformedURLException e) {
                throw new ParameterException(MessageFormat.format("Invalid URL format {0}", url));
            }
        }
    }
}
