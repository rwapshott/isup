package uk.co.gencoreoperative.docker.isup;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Able to perform a query against a URL and indicate if the server if up.
 */
public class IsUp {
    // How long to wait whilst trying to connect to the server.
    private final int timeout;

    // Required for background/interruptable execution.
    private final ExecutorService service = Executors.newCachedThreadPool();

    public IsUp(int timeout) {
        this.timeout = timeout;
    }

    /**
     * Check if the resource defined by URL is available.
     *
     * @param url Non null url of a resource. May not be available.
     * @return A future which will contain the result of processing.
     * @throws IllegalArgumentException
     */
    public Future<Result> isUp(URL url) {
        Result result = new Result();
        return service.submit(getQueryTask(url, result), result);
    }

    /**
     * Only consider a server to be up if it returns a 200 status code.
     *
     * @param url Non null.
     * @param result Non null container for the result.
     * @return Runnable to schedule for execution.
     */
    private Runnable getQueryTask(final URL url, final Result result) {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setConnectTimeout(timeout);
                    connection.connect();
                    if (connection.getResponseCode() == 200) {
                        result.success();
                    } else {
                        result.failed();
                    }
                } catch (IOException e) {
                    result.error();
                }
            }
        };

    }
}
