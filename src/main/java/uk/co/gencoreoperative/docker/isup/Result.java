package uk.co.gencoreoperative.docker.isup;

/**
 * Result of a check against a server. This result can contain one of a number of states
 * which are intended to be self-documenting.
 */
public class Result {
    public static final String OK = "OK";
    public static final String FAILED = "FAILED";
    public static final String TIMEOUT = "TIMEOUT";
    public static final String ERROR = "ERROR";

    private String state;

    public void success() {
        state = OK;
    }

    public void failed() {
        state = FAILED;
    }

    public void error() {
        state = ERROR;
    }

    public void timeout() {
        state = TIMEOUT;
    }

    public String getState() {
        return state;
    }

    public boolean isSuccess() {
        return OK.equals(getState());
    }

    @Override
    public String toString() {
        return getState();
    }
}
